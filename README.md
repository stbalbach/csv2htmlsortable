csv2htmlsortable.awk
=========

Convert a spreadsheet .csv to a sortable HTML table. Uses sorttable.js by Stuart Langridge 
available at: 

	http://www.kryogenix.org/code/browser/sorttable/

Written in GNU Awk 4.1. GNU Awk is standard on most Unix, for others:

        Windows
        http://gnuwin32.sourceforge.net/packages/gawk.htm
        Mac OS X
        https://code.google.com/p/rudix/wiki/gawk

Instructions:

	csv2htmlsortable.awk infile.csv outfile.html

Example output:

	See sample-output.html

        Click the column heads to sort the table.
