#!/usr/bin/awk -f
#--------------------------------------------------------------------------------
# csv2htmlsorted.awk by Stephen Balbach (stephen@balbach.net)
#
# Purpose: Convert a spreadsheet (.csv) to a sortable HTML table
#
# Requirements: GNU Awk and sorttable.js freely available at:
# http://www.kryogenix.org/code/browser/sorttable/
#
# -------------------------------------------------------------------------------

BEGIN {

  if(ARGC != 3) {
    print "Usage: csv2html infile.csv output.html"
    exit
  }

  output = ARGV[2]

  print "<!DOCTYPE html>" > output
  print "<html>" >> output
  print "<head>" >> output
  print "<script type=\"text/javascript\" src=\"sorttable.js\"></script>" >> output
  print "<style type=\"text/css\">" >> output
  print "th, td {" >> output
  print "  padding: 3px !important; " >> output
  print "}" >> output
  print "table.sortable thead {" >> output
  print "  background-color:#333;" >> output
  print "  color:#666666;" >> output
  print "  font-weight: bold;" >> output
  print "  cursor: default;" >> output
  print "}" >> output
  print "th {" >> output
  print "  font-size: 80%;" >> output
  print "}" >> output
  print "td {" >> output
  print "  font-size: 70%;" >> output
  print "}" >> output
  print "</style>" >> output
  print "</head>" >> output
  print "<body>" >> output
  print "" >> output
  print "<table class=\"sortable\">"  >> output
  print "  <tr>" >> output

  while ((getline line < ARGV[1]) > 0) {
    n = patsplit(line, fields, "([^,]*)|(\"[^\"]+\")" )
    for( j=1 ; j<=n ;j++ ) {
      if (substr(fields[j], 1, 1) == "\"") fields[j] = substr(fields[j], 2, length(fields[j]) - 2) # remove lead/trail ""   
      if (substr(fields[j], 1, 1) == "$") fields[j] = substr(fields[j], 2, length(fields[j]) - 2) # remoave lead/trail '$'   
      printf("<td>%s</td>",fields[j]) >> output
    }
    print "</tr>" >> output
  }
  print "</table>" >> output
  print "</body>" >> output
  print "</html>" >> output

}
